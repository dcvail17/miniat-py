from ctypes import *
import priv_defines as const
import headers

libc = CDLL("libc.so.6")
libminiat = CDLL("libminiat.so")
libterm = CDLL("libsimple_term.so")
libkb = CDLL("libsimple_kb.so")

class m_miniat(Structure):
    _fields_ = [("mem", c_uint32 * const.M_MEMORY_SIZE ),
            ("reg", c_uint32 * (const.M_NUM_REGISTERS + const.M_PC_QUEUE_SIZE -1)),
            ("pc", headers.m_pc),
            ("interrupts", headers.m_interrupts),
            ("timers", headers.m_timer),
            ("pipeline", headers.m_pipeline),
            ("bus", headers.m_bus),
            ("csr", POINTER(headers.m_csr)),
            ("ticks", c_uint32)]

class p_simple_term(Structure):
    _fields_ = [("connected", c_int),
                ("address", c_uint32),
                ("bus", POINTER(headers.m_bus))]

class p_simple_keyb(Structure):
    _fields_ = [("connected", c_int),
                ("address", c_uint32),
                ("bus", POINTER(headers.m_bus))]

# Core miniat methods
def miniat_new(f):
    fp = libc.fopen(f, 'r')
    libminiat.miniat_new.restype = POINTER(m_miniat)
    miniat = libminiat.miniat_new(fp)
    return miniat

def miniat_free(m):
    libminiat.miniat_free(m)

def miniat_clock(miniat):
    libminiat.miniat_clock(miniat)

def miniat_reset(m):
    libminiat.miniat_reset(m)

def miniat_pins_bus_get(miniat):
    libminiat.miniat_pins_bus_get.restype = headers.m_bus
    bus = libminiat.miniat_pins_bus_get(miniat)
    return bus

def miniat_pins_bus_set(miniat, bus_state):
    libminiat.miniat_pins_bus_get(miniat, bus_state)

def miniat_pins_set_gpio_port(m, port, pins):
    libminiat.miniat_pins_set_gpio_port(m, port, pins)

def miniat_pins_get_gpio_port(m, port):
    libminiat.miniat_pins_get_gpio_port.restype = headers.m_wyde
    gpio = libminiat.miniat_pins_get_gpio_port(m, port)
    return gpio

# Terminal peripheral
def p_simple_term_new(address):
    libterm.p_simple_term_new.restype = POINTER(p_simple_term)
    term = libterm.p_simple_term_new(address)
    return term

def p_simple_term_set_bus(term, bus_state):
    libterm.p_simple_term_set_bus(term, bus_state)

def p_simple_term_get_bus(term):
    libterm.p_simple_term_get_bus.restype = headers.m_bus
    bus = libterm.p_simple_term_get_bus(term)
    return bus

def p_simple_term_clock(term):
    libterm.p_simple_term_clock(term)

# Keyboard peripheral
def p_simple_kb_new(address):
    libkb.p_simple_kb_new.restype = POINTER(p_simple_keyb)
    kb = libkb.p_simple_kb_new(address)
    return kb

def p_simple_kb_set_bus(kb, bus_state):
    libkb.p_simple_kb_set_bus(kb, bus_state)

def p_simple_kb_get_bus(kb):
    libkb.p_simple_kb_get_bus.restype = headers.m_bus
    bus = libkb.p_simple_kb_get_bus(kb)
    return bus

def p_simple_kb_clock(kb):
    libkb.p_simple_kb_clock(kb)
