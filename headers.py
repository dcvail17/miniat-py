'''Wrapper for miniat_error.h

Generated with:
ctypesgen.py -llibminiat.so /home/dave/spring14/0x1-miniat/vm/src/miniat_error.h /home/dave/spring14/0x1-miniat/vm/src/miniat.h /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_bus.h /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_decode.h /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_enums.h /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_execute.h /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_fetch.h /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_file.h /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_hazards.h /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_interrupts.h /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_memory.h /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_pc.h /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_pipeline.h /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_registers.h /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_timers.h /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_unions.h /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_util.h /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_writeback.h -o headers.py

Do not modify this file.
'''

__docformat__ =  'restructuredtext'

# Begin preamble

import ctypes, os, sys
from ctypes import *

_int_types = (c_int16, c_int32)
if hasattr(ctypes, 'c_int64'):
    # Some builds of ctypes apparently do not have c_int64
    # defined; it's a pretty good bet that these builds do not
    # have 64-bit pointers.
    _int_types += (c_int64,)
for t in _int_types:
    if sizeof(t) == sizeof(c_size_t):
        c_ptrdiff_t = t
del t
del _int_types

class c_void(Structure):
    # c_void_p is a buggy return type, converting to int, so
    # POINTER(None) == c_void_p is actually written as
    # POINTER(c_void), so it can be treated as a real pointer.
    _fields_ = [('dummy', c_int)]

def POINTER(obj):
    p = ctypes.POINTER(obj)

    # Convert None to a real NULL pointer to work around bugs
    # in how ctypes handles None on 64-bit platforms
    if not isinstance(p.from_param, classmethod):
        def from_param(cls, x):
            if x is None:
                return cls()
            else:
                return x
        p.from_param = classmethod(from_param)

    return p

class UserString:
    def __init__(self, seq):
        if isinstance(seq, basestring):
            self.data = seq
        elif isinstance(seq, UserString):
            self.data = seq.data[:]
        else:
            self.data = str(seq)
    def __str__(self): return str(self.data)
    def __repr__(self): return repr(self.data)
    def __int__(self): return int(self.data)
    def __long__(self): return long(self.data)
    def __float__(self): return float(self.data)
    def __complex__(self): return complex(self.data)
    def __hash__(self): return hash(self.data)

    def __cmp__(self, string):
        if isinstance(string, UserString):
            return cmp(self.data, string.data)
        else:
            return cmp(self.data, string)
    def __contains__(self, char):
        return char in self.data

    def __len__(self): return len(self.data)
    def __getitem__(self, index): return self.__class__(self.data[index])
    def __getslice__(self, start, end):
        start = max(start, 0); end = max(end, 0)
        return self.__class__(self.data[start:end])

    def __add__(self, other):
        if isinstance(other, UserString):
            return self.__class__(self.data + other.data)
        elif isinstance(other, basestring):
            return self.__class__(self.data + other)
        else:
            return self.__class__(self.data + str(other))
    def __radd__(self, other):
        if isinstance(other, basestring):
            return self.__class__(other + self.data)
        else:
            return self.__class__(str(other) + self.data)
    def __mul__(self, n):
        return self.__class__(self.data*n)
    __rmul__ = __mul__
    def __mod__(self, args):
        return self.__class__(self.data % args)

    # the following methods are defined in alphabetical order:
    def capitalize(self): return self.__class__(self.data.capitalize())
    def center(self, width, *args):
        return self.__class__(self.data.center(width, *args))
    def count(self, sub, start=0, end=sys.maxint):
        return self.data.count(sub, start, end)
    def decode(self, encoding=None, errors=None): # XXX improve this?
        if encoding:
            if errors:
                return self.__class__(self.data.decode(encoding, errors))
            else:
                return self.__class__(self.data.decode(encoding))
        else:
            return self.__class__(self.data.decode())
    def encode(self, encoding=None, errors=None): # XXX improve this?
        if encoding:
            if errors:
                return self.__class__(self.data.encode(encoding, errors))
            else:
                return self.__class__(self.data.encode(encoding))
        else:
            return self.__class__(self.data.encode())
    def endswith(self, suffix, start=0, end=sys.maxint):
        return self.data.endswith(suffix, start, end)
    def expandtabs(self, tabsize=8):
        return self.__class__(self.data.expandtabs(tabsize))
    def find(self, sub, start=0, end=sys.maxint):
        return self.data.find(sub, start, end)
    def index(self, sub, start=0, end=sys.maxint):
        return self.data.index(sub, start, end)
    def isalpha(self): return self.data.isalpha()
    def isalnum(self): return self.data.isalnum()
    def isdecimal(self): return self.data.isdecimal()
    def isdigit(self): return self.data.isdigit()
    def islower(self): return self.data.islower()
    def isnumeric(self): return self.data.isnumeric()
    def isspace(self): return self.data.isspace()
    def istitle(self): return self.data.istitle()
    def isupper(self): return self.data.isupper()
    def join(self, seq): return self.data.join(seq)
    def ljust(self, width, *args):
        return self.__class__(self.data.ljust(width, *args))
    def lower(self): return self.__class__(self.data.lower())
    def lstrip(self, chars=None): return self.__class__(self.data.lstrip(chars))
    def partition(self, sep):
        return self.data.partition(sep)
    def replace(self, old, new, maxsplit=-1):
        return self.__class__(self.data.replace(old, new, maxsplit))
    def rfind(self, sub, start=0, end=sys.maxint):
        return self.data.rfind(sub, start, end)
    def rindex(self, sub, start=0, end=sys.maxint):
        return self.data.rindex(sub, start, end)
    def rjust(self, width, *args):
        return self.__class__(self.data.rjust(width, *args))
    def rpartition(self, sep):
        return self.data.rpartition(sep)
    def rstrip(self, chars=None): return self.__class__(self.data.rstrip(chars))
    def split(self, sep=None, maxsplit=-1):
        return self.data.split(sep, maxsplit)
    def rsplit(self, sep=None, maxsplit=-1):
        return self.data.rsplit(sep, maxsplit)
    def splitlines(self, keepends=0): return self.data.splitlines(keepends)
    def startswith(self, prefix, start=0, end=sys.maxint):
        return self.data.startswith(prefix, start, end)
    def strip(self, chars=None): return self.__class__(self.data.strip(chars))
    def swapcase(self): return self.__class__(self.data.swapcase())
    def title(self): return self.__class__(self.data.title())
    def translate(self, *args):
        return self.__class__(self.data.translate(*args))
    def upper(self): return self.__class__(self.data.upper())
    def zfill(self, width): return self.__class__(self.data.zfill(width))

class MutableString(UserString):
    """mutable string objects

    Python strings are immutable objects.  This has the advantage, that
    strings may be used as dictionary keys.  If this property isn't needed
    and you insist on changing string values in place instead, you may cheat
    and use MutableString.

    But the purpose of this class is an educational one: to prevent
    people from inventing their own mutable string class derived
    from UserString and than forget thereby to remove (override) the
    __hash__ method inherited from UserString.  This would lead to
    errors that would be very hard to track down.

    A faster and better solution is to rewrite your program using lists."""
    def __init__(self, string=""):
        self.data = string
    def __hash__(self):
        raise TypeError("unhashable type (it is mutable)")
    def __setitem__(self, index, sub):
        if index < 0:
            index += len(self.data)
        if index < 0 or index >= len(self.data): raise IndexError
        self.data = self.data[:index] + sub + self.data[index+1:]
    def __delitem__(self, index):
        if index < 0:
            index += len(self.data)
        if index < 0 or index >= len(self.data): raise IndexError
        self.data = self.data[:index] + self.data[index+1:]
    def __setslice__(self, start, end, sub):
        start = max(start, 0); end = max(end, 0)
        if isinstance(sub, UserString):
            self.data = self.data[:start]+sub.data+self.data[end:]
        elif isinstance(sub, basestring):
            self.data = self.data[:start]+sub+self.data[end:]
        else:
            self.data =  self.data[:start]+str(sub)+self.data[end:]
    def __delslice__(self, start, end):
        start = max(start, 0); end = max(end, 0)
        self.data = self.data[:start] + self.data[end:]
    def immutable(self):
        return UserString(self.data)
    def __iadd__(self, other):
        if isinstance(other, UserString):
            self.data += other.data
        elif isinstance(other, basestring):
            self.data += other
        else:
            self.data += str(other)
        return self
    def __imul__(self, n):
        self.data *= n
        return self

class String(MutableString, Union):

    _fields_ = [('raw', POINTER(c_char)),
                ('data', c_char_p)]

    def __init__(self, obj=""):
        if isinstance(obj, (str, unicode, UserString)):
            self.data = str(obj)
        else:
            self.raw = obj

    def __len__(self):
        return self.data and len(self.data) or 0

    def from_param(cls, obj):
        # Convert None or 0
        if obj is None or obj == 0:
            return cls(POINTER(c_char)())

        # Convert from String
        elif isinstance(obj, String):
            return obj

        # Convert from str
        elif isinstance(obj, str):
            return cls(obj)

        # Convert from c_char_p
        elif isinstance(obj, c_char_p):
            return obj

        # Convert from POINTER(c_char)
        elif isinstance(obj, POINTER(c_char)):
            return obj

        # Convert from raw pointer
        elif isinstance(obj, int):
            return cls(cast(obj, POINTER(c_char)))

        # Convert from object
        else:
            return String.from_param(obj._as_parameter_)
    from_param = classmethod(from_param)

def ReturnString(obj, func=None, arguments=None):
    return String.from_param(obj)

# As of ctypes 1.0, ctypes does not support custom error-checking
# functions on callbacks, nor does it support custom datatypes on
# callbacks, so we must ensure that all callbacks return
# primitive datatypes.
#
# Non-primitive return values wrapped with UNCHECKED won't be
# typechecked, and will be converted to c_void_p.
def UNCHECKED(type):
    if (hasattr(type, "_type_") and isinstance(type._type_, str)
        and type._type_ != "P"):
        return type
    else:
        return c_void_p

# ctypes doesn't have direct support for variadic functions, so we have to write
# our own wrapper class
class _variadic_function(object):
    def __init__(self,func,restype,argtypes):
        self.func=func
        self.func.restype=restype
        self.argtypes=argtypes
    def _as_parameter_(self):
        # So we can pass this variadic function as a function pointer
        return self.func
    def __call__(self,*args):
        fixed_args=[]
        i=0
        for argtype in self.argtypes:
            # Typecheck what we can
            fixed_args.append(argtype.from_param(args[i]))
            i+=1
        return self.func(*fixed_args+list(args[i:]))

# End preamble

_libs = {}
_libdirs = []

# Begin loader

# ----------------------------------------------------------------------------
# Copyright (c) 2008 David James
# Copyright (c) 2006-2008 Alex Holkner
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
#  * Neither the name of pyglet nor the names of its
#    contributors may be used to endorse or promote products
#    derived from this software without specific prior written
#    permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
# ----------------------------------------------------------------------------

import os.path, re, sys, glob
import ctypes
import ctypes.util

def _environ_path(name):
    if name in os.environ:
        return os.environ[name].split(":")
    else:
        return []

class LibraryLoader(object):
    def __init__(self):
        self.other_dirs=[]

    def load_library(self,libname):
        """Given the name of a library, load it."""
        paths = self.getpaths(libname)

        for path in paths:
            if os.path.exists(path):
                return self.load(path)

        raise ImportError("%s not found." % libname)

    def load(self,path):
        """Given a path to a library, load it."""
        try:
            # Darwin requires dlopen to be called with mode RTLD_GLOBAL instead
            # of the default RTLD_LOCAL.  Without this, you end up with
            # libraries not being loadable, resulting in "Symbol not found"
            # errors
            if sys.platform == 'darwin':
                return ctypes.CDLL(path, ctypes.RTLD_GLOBAL)
            else:
                return ctypes.cdll.LoadLibrary(path)
        except OSError,e:
            raise ImportError(e)

    def getpaths(self,libname):
        """Return a list of paths where the library might be found."""
        if os.path.isabs(libname):
            yield libname

        else:
            for path in self.getplatformpaths(libname):
                yield path

            path = ctypes.util.find_library(libname)
            if path: yield path

    def getplatformpaths(self, libname):
        return []

# Darwin (Mac OS X)

class DarwinLibraryLoader(LibraryLoader):
    name_formats = ["lib%s.dylib", "lib%s.so", "lib%s.bundle", "%s.dylib",
                "%s.so", "%s.bundle", "%s"]

    def getplatformpaths(self,libname):
        if os.path.pathsep in libname:
            names = [libname]
        else:
            names = [format % libname for format in self.name_formats]

        for dir in self.getdirs(libname):
            for name in names:
                yield os.path.join(dir,name)

    def getdirs(self,libname):
        '''Implements the dylib search as specified in Apple documentation:

        http://developer.apple.com/documentation/DeveloperTools/Conceptual/
            DynamicLibraries/Articles/DynamicLibraryUsageGuidelines.html

        Before commencing the standard search, the method first checks
        the bundle's ``Frameworks`` directory if the application is running
        within a bundle (OS X .app).
        '''

        dyld_fallback_library_path = _environ_path("DYLD_FALLBACK_LIBRARY_PATH")
        if not dyld_fallback_library_path:
            dyld_fallback_library_path = [os.path.expanduser('~/lib'),
                                          '/usr/local/lib', '/usr/lib']

        dirs = []

        if '/' in libname:
            dirs.extend(_environ_path("DYLD_LIBRARY_PATH"))
        else:
            dirs.extend(_environ_path("LD_LIBRARY_PATH"))
            dirs.extend(_environ_path("DYLD_LIBRARY_PATH"))

        dirs.extend(self.other_dirs)
        dirs.append(".")

        if hasattr(sys, 'frozen') and sys.frozen == 'macosx_app':
            dirs.append(os.path.join(
                os.environ['RESOURCEPATH'],
                '..',
                'Frameworks'))

        dirs.extend(dyld_fallback_library_path)

        return dirs

# Posix

class PosixLibraryLoader(LibraryLoader):
    _ld_so_cache = None

    def _create_ld_so_cache(self):
        # Recreate search path followed by ld.so.  This is going to be
        # slow to build, and incorrect (ld.so uses ld.so.cache, which may
        # not be up-to-date).  Used only as fallback for distros without
        # /sbin/ldconfig.
        #
        # We assume the DT_RPATH and DT_RUNPATH binary sections are omitted.

        directories = []
        for name in ("LD_LIBRARY_PATH",
                     "SHLIB_PATH", # HPUX
                     "LIBPATH", # OS/2, AIX
                     "LIBRARY_PATH", # BE/OS
                    ):
            if name in os.environ:
                directories.extend(os.environ[name].split(os.pathsep))
        directories.extend(self.other_dirs)
        directories.append(".")

        try: directories.extend([dir.strip() for dir in open('/etc/ld.so.conf')])
        except IOError: pass

        directories.extend(['/lib', '/usr/lib', '/lib64', '/usr/lib64'])

        cache = {}
        lib_re = re.compile(r'lib(.*)\.s[ol]')
        ext_re = re.compile(r'\.s[ol]$')
        for dir in directories:
            try:
                for path in glob.glob("%s/*.s[ol]*" % dir):
                    file = os.path.basename(path)

                    # Index by filename
                    if file not in cache:
                        cache[file] = path

                    # Index by library name
                    match = lib_re.match(file)
                    if match:
                        library = match.group(1)
                        if library not in cache:
                            cache[library] = path
            except OSError:
                pass

        self._ld_so_cache = cache

    def getplatformpaths(self, libname):
        if self._ld_so_cache is None:
            self._create_ld_so_cache()

        result = self._ld_so_cache.get(libname)
        if result: yield result

        path = ctypes.util.find_library(libname)
        if path: yield os.path.join("/lib",path)

# Windows

class _WindowsLibrary(object):
    def __init__(self, path):
        self.cdll = ctypes.cdll.LoadLibrary(path)
        self.windll = ctypes.windll.LoadLibrary(path)

    def __getattr__(self, name):
        try: return getattr(self.cdll,name)
        except AttributeError:
            try: return getattr(self.windll,name)
            except AttributeError:
                raise

class WindowsLibraryLoader(LibraryLoader):
    name_formats = ["%s.dll", "lib%s.dll", "%slib.dll"]

    def load_library(self, libname):
        try:
            result = LibraryLoader.load_library(self, libname)
        except ImportError:
            result = None
            if os.path.sep not in libname:
                for name in self.name_formats:
                    try:
                        result = getattr(ctypes.cdll, name % libname)
                        if result:
                            break
                    except WindowsError:
                        result = None
            if result is None:
                try:
                    result = getattr(ctypes.cdll, libname)
                except WindowsError:
                    result = None
            if result is None:
                raise ImportError("%s not found." % libname)
        return result

    def load(self, path):
        return _WindowsLibrary(path)

    def getplatformpaths(self, libname):
        if os.path.sep not in libname:
            for name in self.name_formats:
                dll_in_current_dir = os.path.abspath(name % libname)
                if os.path.exists(dll_in_current_dir):
                    yield dll_in_current_dir
                path = ctypes.util.find_library(name % libname)
                if path:
                    yield path

# Platform switching

# If your value of sys.platform does not appear in this dict, please contact
# the Ctypesgen maintainers.

loaderclass = {
    "darwin":   DarwinLibraryLoader,
    "cygwin":   WindowsLibraryLoader,
    "win32":    WindowsLibraryLoader
}

loader = loaderclass.get(sys.platform, PosixLibraryLoader)()

def add_library_search_dirs(other_dirs):
    loader.other_dirs = other_dirs

load_library = loader.load_library

del loaderclass

# End loader

add_library_search_dirs([])

# Begin libraries

_libs["libminiat.so"] = load_library("libminiat.so")

# 1 libraries
# End libraries

# No modules

__off_t = c_long # /usr/include/bits/types.h: 131

__off64_t = c_long # /usr/include/bits/types.h: 132

__jmp_buf = c_long * 8 # /usr/include/bits/setjmp.h: 31

# /usr/include/bits/sigset.h: 30
class struct_anon_2(Structure):
    pass

struct_anon_2.__slots__ = [
    '__val',
]
struct_anon_2._fields_ = [
    ('__val', c_ulong * (1024 / (8 * sizeof(c_ulong)))),
]

__sigset_t = struct_anon_2 # /usr/include/bits/sigset.h: 30

# /usr/include/setjmp.h: 34
class struct___jmp_buf_tag(Structure):
    pass

struct___jmp_buf_tag.__slots__ = [
    '__jmpbuf',
    '__mask_was_saved',
    '__saved_mask',
]
struct___jmp_buf_tag._fields_ = [
    ('__jmpbuf', __jmp_buf),
    ('__mask_was_saved', c_int),
    ('__saved_mask', __sigset_t),
]

jmp_buf = struct___jmp_buf_tag * 1 # /usr/include/setjmp.h: 48

# /usr/include/libio.h: 245
class struct__IO_FILE(Structure):
    pass

_IO_lock_t = None # /usr/include/libio.h: 154

# /usr/include/libio.h: 160
class struct__IO_marker(Structure):
    pass

struct__IO_marker.__slots__ = [
    '_next',
    '_sbuf',
    '_pos',
]
struct__IO_marker._fields_ = [
    ('_next', POINTER(struct__IO_marker)),
    ('_sbuf', POINTER(struct__IO_FILE)),
    ('_pos', c_int),
]

struct__IO_FILE.__slots__ = [
    '_flags',
    '_IO_read_ptr',
    '_IO_read_end',
    '_IO_read_base',
    '_IO_write_base',
    '_IO_write_ptr',
    '_IO_write_end',
    '_IO_buf_base',
    '_IO_buf_end',
    '_IO_save_base',
    '_IO_backup_base',
    '_IO_save_end',
    '_markers',
    '_chain',
    '_fileno',
    '_flags2',
    '_old_offset',
    '_cur_column',
    '_vtable_offset',
    '_shortbuf',
    '_lock',
    '_offset',
    '__pad1',
    '__pad2',
    '__pad3',
    '__pad4',
    '__pad5',
    '_mode',
    '_unused2',
]
struct__IO_FILE._fields_ = [
    ('_flags', c_int),
    ('_IO_read_ptr', String),
    ('_IO_read_end', String),
    ('_IO_read_base', String),
    ('_IO_write_base', String),
    ('_IO_write_ptr', String),
    ('_IO_write_end', String),
    ('_IO_buf_base', String),
    ('_IO_buf_end', String),
    ('_IO_save_base', String),
    ('_IO_backup_base', String),
    ('_IO_save_end', String),
    ('_markers', POINTER(struct__IO_marker)),
    ('_chain', POINTER(struct__IO_FILE)),
    ('_fileno', c_int),
    ('_flags2', c_int),
    ('_old_offset', __off_t),
    ('_cur_column', c_ushort),
    ('_vtable_offset', c_char),
    ('_shortbuf', c_char * 1),
    ('_lock', POINTER(_IO_lock_t)),
    ('_offset', __off64_t),
    ('__pad1', POINTER(None)),
    ('__pad2', POINTER(None)),
    ('__pad3', POINTER(None)),
    ('__pad4', POINTER(None)),
    ('__pad5', c_size_t),
    ('_mode', c_int),
    ('_unused2', c_char * (((15 * sizeof(c_int)) - (4 * sizeof(POINTER(None)))) - sizeof(c_size_t))),
]

# /usr/include/stdio.h: 170
try:
    stderr = (POINTER(struct__IO_FILE)).in_dll(_libs['libminiat.so'], 'stderr')
except:
    pass

enum_m_error_num = c_int # /home/dave/spring14/0x1-miniat/vm/src/miniat_error.h: 15

m_error_num = enum_m_error_num # /home/dave/spring14/0x1-miniat/vm/src/miniat_error.h: 15

# /home/dave/spring14/0x1-miniat/vm/src/miniat_error.h: 174
if hasattr(_libs['libminiat.so'], 'miniat_dump_error'):
    miniat_dump_error = _libs['libminiat.so'].miniat_dump_error
    miniat_dump_error.argtypes = []
    miniat_dump_error.restype = None

# /home/dave/spring14/0x1-miniat/vm/src/miniat_error.h: 188
if hasattr(_libs['libminiat.so'], 'miniat_panic'):
    miniat_panic = _libs['libminiat.so'].miniat_panic
    miniat_panic.argtypes = [c_int]
    miniat_panic.restype = None

# /home/dave/spring14/0x1-miniat/vm/src/miniat_error.h: 270
class struct_m_error(Structure):
    pass

m_error = struct_m_error # /home/dave/spring14/0x1-miniat/vm/src/miniat_error.h: 233

# /home/dave/spring14/0x1-miniat/vm/src/miniat_error.h: 250
class struct_m_error_stack(Structure):
    pass

m_error_stack = struct_m_error_stack # /home/dave/spring14/0x1-miniat/vm/src/miniat_error.h: 234

# /home/dave/spring14/0x1-miniat/vm/src/miniat_error.h: 258
class struct_m_error_details(Structure):
    pass

m_error_details = struct_m_error_details # /home/dave/spring14/0x1-miniat/vm/src/miniat_error.h: 235

enum_m_error_stack_action = c_int # /home/dave/spring14/0x1-miniat/vm/src/miniat_error.h: 236

m_error_stack_action = enum_m_error_stack_action # /home/dave/spring14/0x1-miniat/vm/src/miniat_error.h: 236

struct_m_error_stack.__slots__ = [
    'jbuf',
    'next',
]
struct_m_error_stack._fields_ = [
    ('jbuf', jmp_buf),
    ('next', POINTER(m_error_stack)),
]

struct_m_error_details.__slots__ = [
    'active',
    'num',
    'line',
    'file',
    'func',
]
struct_m_error_details._fields_ = [
    ('active', c_int),
    ('num', m_error_num),
    ('line', c_int),
    ('file', String),
    ('func', String),
]

struct_m_error.__slots__ = [
    'head',
    'curr_err',
]
struct_m_error._fields_ = [
    ('head', POINTER(m_error_stack)),
    ('curr_err', m_error_details),
]

# /home/dave/spring14/0x1-miniat/vm/src/miniat_error.h: 288
for _lib in _libs.values():
    try:
        __m_err__ = (m_error).in_dll(_lib, '__m_err__')
        break
    except:
        pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_error.h: 298
for _lib in _libs.values():
    try:
        __m_g_err__ = (m_error).in_dll(_lib, '__m_g_err__')
        break
    except:
        pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_error.h: 307
if hasattr(_libs['libminiat.so'], '__error_pop'):
    __error_pop = _libs['libminiat.so'].__error_pop
    __error_pop.argtypes = [m_error_stack_action]
    __error_pop.restype = POINTER(m_error_stack)

# /home/dave/spring14/0x1-miniat/vm/src/miniat_error.h: 320
if hasattr(_libs['libminiat.so'], '__error_push'):
    __error_push = _libs['libminiat.so'].__error_push
    __error_push.argtypes = [POINTER(m_error_stack)]
    __error_push.restype = None

# /home/dave/spring14/0x1-miniat/vm/src/miniat_error.h: 333
if hasattr(_libs['libminiat.so'], '__set_active_error'):
    __set_active_error = _libs['libminiat.so'].__set_active_error
    __set_active_error.argtypes = [c_int, String, String, m_error_num]
    __set_active_error.restype = None

m_sbyte = c_int8 # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 23

m_ubyte = c_uint8 # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 24

m_swyde = c_int16 # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 27

m_uwyde = c_uint16 # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 28

m_sword = c_int32 # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 31

m_uword = c_uint32 # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 32

m_sdword = c_int64 # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 34

m_udword = c_uint64 # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 35

WRONG_BYTE_SIZE_SBYTE = c_int * (((sizeof(m_sbyte) == 1) * 2) - 1) # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 44

WRONG_BYTE_SIZE_UBYTE = c_int * (((sizeof(m_ubyte) == 1) * 2) - 1) # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 45

WRONG_BYTE_SIZE_SWYDE = c_int * (((sizeof(m_swyde) == 2) * 2) - 1) # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 47

WRONG_BYTE_SIZE_UWYDE = c_int * (((sizeof(m_uwyde) == 2) * 2) - 1) # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 48

WRONG_BYTE_SIZE_SWORD = c_int * (((sizeof(m_sword) == 4) * 2) - 1) # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 50

WRONG_BYTE_SIZE_UWORD = c_int * (((sizeof(m_uword) == 4) * 2) - 1) # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 51

WRONG_BYTE_SIZE_SDWORD = c_int * (((sizeof(m_sdword) == 8) * 2) - 1) # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 53

WRONG_BYTE_SIZE_UDWORD = c_int * (((sizeof(m_udword) == 8) * 2) - 1) # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 54

# /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 76
class struct_m_bus(Structure):
    pass

m_bus = struct_m_bus # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 57

# /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 96
class union_m_byte(Union):
    pass

m_byte = union_m_byte # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 59

# /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 116
class union_m_wyde(Union):
    pass

m_wyde = union_m_wyde # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 60

# /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 145
class union_m_word(Union):
    pass

m_word = union_m_word # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 61

# /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 188
class union_m_dword(Union):
    pass

m_dword = union_m_dword # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 62

# /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 196
class union_m_timers_io(Union):
    pass

m_timers_io = union_m_timers_io # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 63

enum_m_bus_state = c_int # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 65

m_bus_state = enum_m_bus_state # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 65

enum_m_gpio_id = c_int # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 66

m_gpio_id = enum_m_gpio_id # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 66

enum_m_timer_id = c_int # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 67

m_timer_id = enum_m_timer_id # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 67

enum_m_xint_id = c_int # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 68

m_xint = enum_m_xint_id # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 68

struct_m_bus.__slots__ = [
    'req',
    'ack',
    'rW',
    'address',
    'data',
]
struct_m_bus._fields_ = [
    ('req', m_ubyte),
    ('ack', m_ubyte),
    ('rW', m_ubyte),
    ('address', m_uword),
    ('data', m_uword),
]

union_m_byte.__slots__ = [
    's',
    'u',
]
union_m_byte._fields_ = [
    ('s', m_sbyte),
    ('u', m_ubyte),
]

union_m_wyde.__slots__ = [
    's',
    'u',
]
union_m_wyde._fields_ = [
    ('s', m_swyde),
    ('u', m_uwyde),
]

union_m_word.__slots__ = [
    's',
    'u',
]
union_m_word._fields_ = [
    ('s', m_sword),
    ('u', m_uword),
]

union_m_dword.__slots__ = [
    's',
    'u',
]
union_m_dword._fields_ = [
    ('s', m_sdword),
    ('u', m_udword),
]

union_m_timers_io.__slots__ = [
    'all',
]
union_m_timers_io._fields_ = [
    ('all', m_ubyte),
]

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 160
class struct_m_pc(Structure):
    pass

m_pc = struct_m_pc # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 7

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 192
class struct_m_ivbt_pc(Structure):
    pass

m_ivbt_pc = struct_m_ivbt_pc # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 8

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 225
class struct_m_interrupts(Structure):
    pass

m_interrupts = struct_m_interrupts # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 9

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 129
class struct_m_timer(Structure):
    pass

m_timer = struct_m_timer # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 10

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 116
class struct_m_pipeline(Structure):
    pass

m_pipeline = struct_m_pipeline # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 11

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_unions.h: 7
class union_m_csr(Union):
    pass

m_csr = union_m_csr # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 13

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 202
class struct_m_ivbt(Structure):
    pass

m_ivbt = struct_m_ivbt # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 15

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 182
class struct_m_priv_bus(Structure):
    pass

m_priv_bus = struct_m_priv_bus # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 17

enum_m_timer_modes = c_int # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 19

m_timer_modes = enum_m_timer_modes # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 19

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_unions.h: 47
class union_m_timer_compare(Union):
    pass

m_timer_compare = union_m_timer_compare # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 20

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_unions.h: 37
class union_m_timer_control(Union):
    pass

m_timer_control = union_m_timer_control # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 21

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 143
class struct_m_watchdog(Structure):
    pass

m_watchdog = struct_m_watchdog # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 25

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_unions.h: 19
class union_m_watchdog_control(Union):
    pass

m_watchdog_control = union_m_watchdog_control # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 26

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_unions.h: 29
class union_m_watchdog_compare(Union):
    pass

m_watchdog_compare = union_m_watchdog_compare # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 27

enum_m_pipeline_stage = c_int # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 29

m_pipeline_stage = enum_m_pipeline_stage # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 29

enum_m_pipeline_stage_state = c_int # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 30

m_pipeline_stage_state = enum_m_pipeline_stage_state # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 30

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 46
class struct_m_pipeline_fetch(Structure):
    pass

m_pipeline_fetch = struct_m_pipeline_fetch # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 32

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 36
class struct_m_pipeline_fetch_latch_out(Structure):
    pass

m_pipeline_fetch_latch_out = struct_m_pipeline_fetch_latch_out # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 33

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 71
class struct_m_pipeline_decode(Structure):
    pass

m_pipeline_decode = struct_m_pipeline_decode # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 35

m_pipeline_decode_latch_in = struct_m_pipeline_fetch_latch_out # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 36

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 57
class struct_m_pipeline_decode_latch_out(Structure):
    pass

m_pipeline_decode_latch_out = struct_m_pipeline_decode_latch_out # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 37

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 95
class struct_m_pipeline_execute(Structure):
    pass

m_pipeline_execute = struct_m_pipeline_execute # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 39

m_pipeline_execute_latch_in = struct_m_pipeline_decode_latch_out # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 40

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 82
class struct_m_pipeline_execute_latch_out(Structure):
    pass

m_pipeline_execute_latch_out = struct_m_pipeline_execute_latch_out # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 41

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 106
class struct_m_pipeline_writeback(Structure):
    pass

m_pipeline_writeback = struct_m_pipeline_writeback # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 43

m_pipeline_writeback_latch_in = struct_m_pipeline_execute_latch_out # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 44

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 28
class struct_m_pipeline_register_hazards(Structure):
    pass

m_pipeline_register_hazards = struct_m_pipeline_register_hazards # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 46

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 19
class struct_m_pipeline_memory_hazards(Structure):
    pass

m_pipeline_memory_hazards = struct_m_pipeline_memory_hazards # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 47

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 216
class struct_m_memory_characteristics(Structure):
    pass

m_memory_characteristics = struct_m_memory_characteristics # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 49

enum_m_memory_type = c_int # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 50

m_memory_type = enum_m_memory_type # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 50

enum_m_memory_permissions = c_int # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 51

m_memory_permissions = enum_m_memory_permissions # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 51

enum_m_block_type = c_int # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 53

m_block_type = enum_m_block_type # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 53

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 243
class struct_m_file_block(Structure):
    pass

m_file_block = struct_m_file_block # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 54

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_unions.h: 55
class union_m_low_word_layout(Union):
    pass

m_low_word_layout = union_m_low_word_layout # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_typedefs.h: 56

union_m_csr.__slots__ = [
    'all',
]
union_m_csr._fields_ = [
    ('all', m_uword),
]

union_m_watchdog_control.__slots__ = [
    'all',
]
union_m_watchdog_control._fields_ = [
    ('all', m_uword),
]

union_m_watchdog_compare.__slots__ = [
    'all',
]
union_m_watchdog_compare._fields_ = [
    ('all', m_uword),
]

union_m_timer_control.__slots__ = [
    'all',
]
union_m_timer_control._fields_ = [
    ('all', m_ubyte),
]

union_m_timer_compare.__slots__ = [
    'all',
]
union_m_timer_compare._fields_ = [
    ('all', m_uword),
]

union_m_low_word_layout.__slots__ = [
    'all',
]
union_m_low_word_layout._fields_ = [
    ('all', m_uword),
]

struct_m_pipeline_memory_hazards.__slots__ = [
    'size',
    'queue',
]
struct_m_pipeline_memory_hazards._fields_ = [
    ('size', m_ubyte),
    ('queue', m_uword * (4 - 1)),
]

struct_m_pipeline_register_hazards.__slots__ = [
    'size',
    'queue',
]
struct_m_pipeline_register_hazards._fields_ = [
    ('size', m_ubyte),
    ('queue', m_uword * (4 - 1)),
]

struct_m_pipeline_fetch_latch_out.__slots__ = [
    'NPC',
    'data',
]
struct_m_pipeline_fetch_latch_out._fields_ = [
    ('NPC', m_uword),
    ('data', m_uword * 2),
]

struct_m_pipeline_fetch.__slots__ = [
    'out',
    'state',
    'duration',
]
struct_m_pipeline_fetch._fields_ = [
    ('out', m_pipeline_fetch_latch_out),
    ('state', m_pipeline_stage_state),
    ('duration', m_uword),
]

struct_m_pipeline_decode_latch_out.__slots__ = [
    'NPC',
    'hint',
    'opcode',
    'A',
    'B',
    'D',
    'rX',
]
struct_m_pipeline_decode_latch_out._fields_ = [
    ('NPC', m_uword),
    ('hint', m_ubyte),
    ('opcode', m_uword),
    ('A', m_uword),
    ('B', m_uword),
    ('D', m_uword),
    ('rX', m_ubyte),
]

struct_m_pipeline_decode.__slots__ = [
    '_in',
    'out',
    'state',
    'duration',
]
struct_m_pipeline_decode._fields_ = [
    ('_in', m_pipeline_decode_latch_in),
    ('out', m_pipeline_decode_latch_out),
    ('state', m_pipeline_stage_state),
    ('duration', m_uword),
]

struct_m_pipeline_execute_latch_out.__slots__ = [
    'NPC',
    'hint',
    'opcode',
    'D',
    'result',
    'rX',
]
struct_m_pipeline_execute_latch_out._fields_ = [
    ('NPC', m_uword),
    ('hint', m_ubyte),
    ('opcode', m_uword),
    ('D', m_uword),
    ('result', m_uword),
    ('rX', m_ubyte),
]

struct_m_pipeline_execute.__slots__ = [
    '_in',
    'out',
    'state',
    'duration',
]
struct_m_pipeline_execute._fields_ = [
    ('_in', m_pipeline_execute_latch_in),
    ('out', m_pipeline_execute_latch_out),
    ('state', m_pipeline_stage_state),
    ('duration', m_uword),
]

struct_m_pipeline_writeback.__slots__ = [
    '_in',
    'state',
    'duration',
]
struct_m_pipeline_writeback._fields_ = [
    ('_in', m_pipeline_writeback_latch_in),
    ('state', m_pipeline_stage_state),
    ('duration', m_uword),
]

struct_m_pipeline.__slots__ = [
    'fetch',
    'decode',
    'execute',
    'writeback',
    'register_hazards',
    'memory_hazards',
]
struct_m_pipeline._fields_ = [
    ('fetch', m_pipeline_fetch),
    ('decode', m_pipeline_decode),
    ('execute', m_pipeline_execute),
    ('writeback', m_pipeline_writeback),
    ('register_hazards', m_pipeline_register_hazards),
    ('memory_hazards', m_pipeline_memory_hazards),
]

struct_m_timer.__slots__ = [
    'compare',
    'control',
    'count',
    'io',
    'internal_count',
    'old_in',
    'low_length',
    'high_length',
]
struct_m_timer._fields_ = [
    ('compare', POINTER(m_timer_compare)),
    ('control', POINTER(m_timer_control)),
    ('count', POINTER(m_uword)),
    ('io', POINTER(m_timers_io)),
    ('internal_count', m_uword),
    ('old_in', m_ubyte),
    ('low_length', m_uword),
    ('high_length', m_uword),
]

struct_m_watchdog.__slots__ = [
    'compare',
    'control',
    'internal_count',
]
struct_m_watchdog._fields_ = [
    ('compare', POINTER(m_watchdog_compare)),
    ('control', POINTER(m_watchdog_control)),
    ('internal_count', m_uword),
]

struct_m_pc.__slots__ = [
    'size',
]
struct_m_pc._fields_ = [
    ('size', m_ubyte),
]

struct_m_priv_bus.__slots__ = [
    'state',
    'storage',
    'strobe_duration',
    'priv',
    'pub',
]
struct_m_priv_bus._fields_ = [
    ('state', m_bus_state),
    ('storage', m_uword),
    ('strobe_duration', m_uword),
    ('priv', m_bus),
    ('pub', m_bus),
]

struct_m_ivbt_pc.__slots__ = [
    'size',
    'queue',
]
struct_m_ivbt_pc._fields_ = [
    ('size', m_ubyte),
    ('queue', m_uword * 4),
]

struct_m_ivbt.__slots__ = [
    'pc',
    'register_hazards',
    'memory_hazards',
    'decode',
    'execute',
    'writeback',
]
struct_m_ivbt._fields_ = [
    ('pc', m_pc),
    ('register_hazards', m_pipeline_register_hazards),
    ('memory_hazards', m_pipeline_memory_hazards),
    ('decode', m_pipeline_decode),
    ('execute', m_pipeline_execute),
    ('writeback', m_pipeline_writeback),
]

struct_m_memory_characteristics.__slots__ = [
    'type',
    'permissions',
    'duration',
]
struct_m_memory_characteristics._fields_ = [
    ('type', m_memory_type),
    ('permissions', m_memory_permissions),
    ('duration', m_ubyte),
]

struct_m_interrupts.__slots__ = [
    'sys_reg',
    'ier_l',
    'ier_h',
    'ifr_l',
    'ifr_h',
    'iqr_l',
    'iqr_h',
    'irr_l',
    'irr_h',
    'oifr_l',
    'oifr_h',
    'ivbt',
]
struct_m_interrupts._fields_ = [
    ('sys_reg', POINTER(m_uword)),
    ('ier_l', POINTER(m_uword)),
    ('ier_h', POINTER(m_uword)),
    ('ifr_l', POINTER(m_uword)),
    ('ifr_h', POINTER(m_uword)),
    ('iqr_l', m_uword),
    ('iqr_h', m_uword),
    ('irr_l', m_uword),
    ('irr_h', m_uword),
    ('oifr_l', m_uword),
    ('oifr_h', m_uword),
    ('ivbt', m_ivbt * (((6937 - 6874) + 1) + 1)),
]

struct_m_file_block.__slots__ = [
    'number',
    'id',
    'start_address',
    'length',
    'data',
]
struct_m_file_block._fields_ = [
    ('number', m_uword),
    ('id', m_uword),
    ('start_address', m_uword),
    ('length', m_uword),
    ('data', POINTER(m_uword)),
]

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_file.h: 94
if hasattr(_libs['libminiat.so'], 'is_big_endian'):
    is_big_endian = _libs['libminiat.so'].is_big_endian
    is_big_endian.argtypes = []
    is_big_endian.restype = c_uint8

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_interrupts.h: 30
if hasattr(_libs['libminiat.so'], 'm_interrupts_priority_scan'):
    m_interrupts_priority_scan = _libs['libminiat.so'].m_interrupts_priority_scan
    m_interrupts_priority_scan.argtypes = [m_uword, m_uword]
    m_interrupts_priority_scan.restype = c_int

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_memory.h: 14
if hasattr(_libs['libminiat.so'], 'm_memory_address_lookup'):
    m_memory_address_lookup = _libs['libminiat.so'].m_memory_address_lookup
    m_memory_address_lookup.argtypes = [m_uword]
    m_memory_address_lookup.restype = m_memory_characteristics

# /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 19
try:
    M_LOW = 0
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 20
try:
    M_HIGH = 1
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 6
try:
    M_HEX_1 = 1
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 10
try:
    M_ADD = 0
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 11
try:
    M_SUB = 1
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 12
try:
    M_MULT = 2
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 13
try:
    M_DIV = 3
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 14
try:
    M_MOD = 4
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 15
try:
    M_AND = 5
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 16
try:
    M_OR = 6
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 17
try:
    M_EXOR = 7
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 18
try:
    M_SHL = 8
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 19
try:
    M_SHR = 9
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 23
try:
    M_LOAD = 16
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 24
try:
    M_STORE = 17
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 25
try:
    M_RLOAD = 18
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 26
try:
    M_RSTORE = 19
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 30
try:
    M_BRAE = 22
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 31
try:
    M_BRANE = 23
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 32
try:
    M_BRAL = 24
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 33
try:
    M_BRALE = 25
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 34
try:
    M_BRAG = 26
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 35
try:
    M_BRAGE = 27
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 39
try:
    M_INT = 28
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 40
try:
    M_IRET = 29
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 45
try:
    M_RAM_INIT_FLAGWORD = 3735928559
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 46
try:
    M_FLASH_INIT_FLAGWORD = 3735928559
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 48
try:
    M_RESERVED_ONE_START = 0
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 49
try:
    M_RESERVED_ONE_END = 511
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 51
try:
    M_RAM_START = 512
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 52
try:
    M_RAM_END = 6655
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 54
try:
    M_RESERVED_TWO_START = 6656
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 55
try:
    M_RESERVED_TWO_END = 6873
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 58
try:
    M_IVT_START = 6874
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 60
try:
    M_IVT_INT_RESET = 6874
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 61
try:
    M_IVT_ILLEGAL_ADDRESS = 6875
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 62
try:
    M_IVT_ILLEGAL_INSTRUCTION = 6876
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 63
try:
    M_IVT_BUS_ERROR = 6877
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 64
try:
    M_IVT_DIVIDE_BY_ZERO = 6878
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 65
try:
    M_IVT_WATCHDOG = 6879
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 66
try:
    M_IVT_TIMER0 = 6880
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 67
try:
    M_IVT_TIMER1 = 6881
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 68
try:
    M_IVT_TIMER2 = 6882
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 69
try:
    M_IVT_TIMER3 = 6883
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 70
try:
    M_IVT_OVERFLOW = 6884
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 72
try:
    M_IVT_RSVD_INT_START = 6938
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 73
try:
    M_IVT_RSVD_INT_END = 6905
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 74
try:
    M_IVT_XINT0 = 6906
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 75
try:
    M_IVT_XINT1 = 6907
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 76
try:
    M_IVT_XINT2 = 6908
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 77
try:
    M_IVT_XINT3 = 6909
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 78
try:
    M_IVT_XINT4 = 6910
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 79
try:
    M_IVT_XINT5 = 6911
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 80
try:
    M_IVT_XINT6 = 6912
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 81
try:
    M_IVT_XINT7 = 6913
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 82
try:
    M_IVT_XINT8 = 6914
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 83
try:
    M_IVT_XINT9 = 6915
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 84
try:
    M_IVT_XINT10 = 6916
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 85
try:
    M_IVT_XINT11 = 6917
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 86
try:
    M_IVT_XINT12 = 6918
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 87
try:
    M_IVT_XINT13 = 6919
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 88
try:
    M_IVT_XINT14 = 6920
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 89
try:
    M_IVT_XINT15 = 6921
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 91
try:
    M_IVT_RSVD_XINT_START = 6922
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 92
try:
    M_IVT_RSVD_XINT_END = 6937
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 93
try:
    M_IVT_END = 6937
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 97
try:
    M_GPIO_START = 6938
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 98
try:
    M_GPIO_PORT_A = 6938
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 99
try:
    M_GPIO_PORT_B = 6939
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 100
try:
    M_GPIO_PORT_C = 6940
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 101
try:
    M_GPIO_PORT_D = 6941
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 102
try:
    M_GPIO_END = 6941
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 106
try:
    M_CSR_START = 6942
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 107
try:
    M_CSR_SYSTEM_REGISTER = 6942
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 108
try:
    M_CSR_WATCHDOG_COMPARE = 6943
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 109
try:
    M_CSR_WATCHDOG_CONTROL = 6944
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 110
try:
    M_CSR_TIMER_CONTROL = 6945
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 111
try:
    M_CSR_TIMER0_COMPARE = 6946
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 112
try:
    M_CSR_TIMER1_COMPARE = 6947
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 113
try:
    M_CSR_TIMER2_COMPARE = 6948
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 114
try:
    M_CSR_TIMER3_COMPARE = 6949
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 115
try:
    M_CSR_TIMER0_COUNT = 6950
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 116
try:
    M_CSR_TIMER1_COUNT = 6951
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 117
try:
    M_CSR_TIMER2_COUNT = 6952
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 118
try:
    M_CSR_TIMER3_COUNT = 6953
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 119
try:
    M_CSR_TIMER_IO = 6954
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 120
try:
    M_CSR_RSVD_START = 6955
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 121
try:
    M_CSR_RSVD_END = 7957
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 123
try:
    M_CSR_PORT_A_DIRECTIONS = 7958
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 124
try:
    M_CSR_PORT_B_DIRECTIONS = 7959
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 125
try:
    M_CSR_PORT_C_DIRECTIONS = 7960
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 126
try:
    M_CSR_PORT_D_DIRECTIONS = 7961
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 128
try:
    M_CSR_INT_ENABLE_LOW = 7962
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 129
try:
    M_CSR_INT_ENABLE_HIGH = 7963
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 130
try:
    M_CSR_INT_FLAG_LOW = 7964
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 131
try:
    M_CSR_INT_FLAG_HIGH = 7965
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 132
try:
    M_CSR_END = 7965
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 136
try:
    M_ID_START = 7966
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 137
try:
    M_ID_END = 8191
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 141
try:
    M_FLASH_START = 8192
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 142
try:
    M_FLASH_END = 16383
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 146
try:
    M_PERIPHERAL_START = 16384
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 147
try:
    M_PERIPHERAL_END = 65535
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 152
try:
    M_REGISTER_ZERO = 0
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 153
try:
    M_REGISTER_PREDICATE = 1
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 154
try:
    M_REGISTER_RSP = 253
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 155
try:
    M_REGISTER_SP = 254
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 156
try:
    M_REGISTER_PC = 255
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 157
try:
    M_REGISTER_PC_ONE = 255
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 158
try:
    M_REGISTER_PC_TWO = 256
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 159
try:
    M_REGISTER_PC_THREE = 257
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 160
try:
    M_PC_QUEUE_SIZE = 4
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 167
try:
    M_IMMEDIATE_START = 0
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 168
try:
    M_IMMEDIATE_END = 31
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 171
try:
    M_REGISTER_C_START = 0
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 172
try:
    M_REGISTER_C_END = 7
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 173
try:
    M_REGISTER_B_START = 8
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 174
try:
    M_REGISTER_B_END = 15
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 175
try:
    M_REGISTER_A_START = 16
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 176
try:
    M_REGISTER_A_END = 23
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 177
try:
    M_OPCODE_START = 24
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 178
try:
    M_OPCODE_END = 28
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 179
try:
    M_UNUSED_START = 29
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 180
try:
    M_UNUSED_END = 29
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 181
try:
    M_HINT_START = 30
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 182
try:
    M_HINT_END = 30
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 183
try:
    M_PREDICATION_START = 31
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 184
try:
    M_PREDICATION_END = 31
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 188
try:
    M_CSR_SYSTEM_REGISTER_INT_BIT = 0
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 189
try:
    M_CSR_SYSTEM_REGISTER_SATURATION_BIT = 1
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 190
try:
    M_CSR_SYSTEM_REGISTER_UNSIGNED_BIT = 2
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 195
try:
    M_INSTRUCTION_SIZE_DWORDS = 1
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 196
try:
    M_INSTRUCTION_SIZE_WORDS = 2
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 197
try:
    M_INSTRUCTION_SIZE_WYDES = 4
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 198
try:
    M_INSTRUCTION_SIZE_BYTES = 8
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 201
try:
    M_MEMORY_SIZE = ((M_PERIPHERAL_END - M_RESERVED_ONE_START) + 1)
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 202
try:
    M_RAM_SIZE = ((M_RAM_END - M_RAM_START) + 1)
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 203
try:
    M_RESERVED_TWO_SIZE = ((M_RESERVED_TWO_END - M_RESERVED_TWO_START) + 1)
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 204
try:
    M_IVT_SIZE = ((M_IVT_END - M_IVT_START) + 1)
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 205
try:
    M_IVT_RSVD_INT_SIZE = ((M_IVT_RSVD_INT_END - M_IVT_RSVD_INT_START) + 1)
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 206
try:
    M_IVT_RSVD_XINT_SIZE = ((M_IVT_RSVD_XINT_END - M_IVT_RSVD_XINT_START) + 1)
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 207
try:
    M_GPIO_SIZE = ((M_GPIO_END - M_GPIO_START) + 1)
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 208
try:
    M_CSR_SIZE = ((M_CSR_END - M_CSR_START) + 1)
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 209
try:
    M_CSR_RSVD_SIZE = ((M_CSR_RSVD_END - M_CSR_RSVD_START) + 1)
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 212
try:
    M_FLASH_SIZE = ((M_FLASH_END - M_FLASH_START) + 1)
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 214
try:
    M_PERIPHERAL_SIZE = ((M_PERIPHERAL_END - M_PERIPHERAL_START) + 1)
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 217
try:
    M_OPCODE_SIZE = ((M_OPCODE_END - M_OPCODE_START) + 1)
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 218
try:
    M_IMMEDIATE_SIZE = ((M_IMMEDIATE_END - M_IMMEDIATE_START) + 1)
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 219
try:
    M_PIPELINE_SIZE = 4
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 220
try:
    M_HAZARD_QUEUE_SIZE = (M_PIPELINE_SIZE - 1)
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 221
try:
    M_IVBT_SIZE = (M_IVT_SIZE + 1)
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 222
try:
    M_IVBT_PC = 64
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 228
try:
    M_INT_RESET = 0
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 229
try:
    M_INT_ILLEGAL_ADDRESS = 1
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 230
try:
    M_INT_ILLEGAL_INSTRUCTION = 2
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 231
try:
    M_INT_BUS_ERROR = 3
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 232
try:
    M_INT_DIVIDE_BY_ZERO = 4
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 233
try:
    M_INT_WATCHDOG = 5
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 234
try:
    M_INT_TIMER0 = 6
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 235
try:
    M_INT_TIMER1 = 7
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 236
try:
    M_INT_TIMER2 = 8
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 237
try:
    M_INT_TIMER3 = 9
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 238
try:
    M_INT_OVERFLOW = 10
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 242
try:
    M_XINT0 = 32
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 243
try:
    M_XINT1 = 33
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 244
try:
    M_XINT2 = 34
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 245
try:
    M_XINT3 = 35
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 246
try:
    M_XINT4 = 36
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 247
try:
    M_XINT5 = 37
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 248
try:
    M_XINT6 = 38
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 249
try:
    M_XINT7 = 39
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 250
try:
    M_XINT8 = 40
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 251
try:
    M_XINT9 = 41
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 252
try:
    M_XINT10 = 42
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 253
try:
    M_XINT11 = 43
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 254
try:
    M_XINT12 = 44
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 255
try:
    M_XINT13 = 45
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 256
try:
    M_XINT14 = 46
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 257
try:
    M_XINT15 = 47
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 262
try:
    M_NUM_REGISTERS = 256
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 263
try:
    M_NUM_OF_TIMERS = 4
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 264
try:
    M_NUM_OF_IFRS = 2
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 268
try:
    M_RAM_STALL_DURATION = 2
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 269
try:
    M_IVT_STALL_DURATION = 0
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 270
try:
    M_GPIO_STALL_DURATION = 0
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 271
try:
    M_CSR_STALL_DURATION = 0
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 272
try:
    M_ID_STALL_DURATION = 2
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 273
try:
    M_FLASH_STALL_DURATION = 4
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 274
try:
    M_PERIPHERAL_STALL_DURATION = 0
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 275
try:
    M_NO_STALL_DURATION = 0
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 279
try:
    M_DEFAULT_LOG_FILE = stderr
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 281
try:
    M_FILE_ID = 3135029470
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 282
try:
    M_BLOCK_SEPERATOR = 3735928559
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 293
def m_bits_in_data(TYPE):
    return (sizeof(TYPE) * 8)

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 294
try:
    M_SYSTEM_BIG_ENDIAN = 0
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 295
try:
    M_SYSTEM_LITTLE_ENDIAN = 1
except:
    pass

# /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_defines.h: 296
try:
    M_BUS_STROBE_DURATION = 2
except:
    pass

m_error = struct_m_error # /home/dave/spring14/0x1-miniat/vm/src/miniat_error.h: 270

m_error_stack = struct_m_error_stack # /home/dave/spring14/0x1-miniat/vm/src/miniat_error.h: 250

m_error_details = struct_m_error_details # /home/dave/spring14/0x1-miniat/vm/src/miniat_error.h: 258

m_bus = struct_m_bus # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 76

m_byte = union_m_byte # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 96

m_wyde = union_m_wyde # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 116

m_word = union_m_word # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 145

m_dword = union_m_dword # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 188

m_timers_io = union_m_timers_io # /home/dave/spring14/0x1-miniat/vm/src/miniat.h: 196

m_pc = struct_m_pc # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 160

m_ivbt_pc = struct_m_ivbt_pc # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 192

m_interrupts = struct_m_interrupts # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 225

m_timer = struct_m_timer # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 129

m_pipeline = struct_m_pipeline # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 116

m_csr = union_m_csr # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_unions.h: 7

m_ivbt = struct_m_ivbt # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 202

m_priv_bus = struct_m_priv_bus # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 182

m_timer_compare = union_m_timer_compare # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_unions.h: 47

m_timer_control = union_m_timer_control # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_unions.h: 37

m_watchdog = struct_m_watchdog # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 143

m_watchdog_control = union_m_watchdog_control # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_unions.h: 19

m_watchdog_compare = union_m_watchdog_compare # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_unions.h: 29

m_pipeline_fetch = struct_m_pipeline_fetch # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 46

m_pipeline_fetch_latch_out = struct_m_pipeline_fetch_latch_out # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 36

m_pipeline_decode = struct_m_pipeline_decode # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 71

m_pipeline_decode_latch_out = struct_m_pipeline_decode_latch_out # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 57

m_pipeline_execute = struct_m_pipeline_execute # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 95

m_pipeline_execute_latch_out = struct_m_pipeline_execute_latch_out # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 82

m_pipeline_writeback = struct_m_pipeline_writeback # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 106

m_pipeline_register_hazards = struct_m_pipeline_register_hazards # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 28

m_pipeline_memory_hazards = struct_m_pipeline_memory_hazards # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 19

m_memory_characteristics = struct_m_memory_characteristics # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 216

m_file_block = struct_m_file_block # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_structures.h: 243

m_low_word_layout = union_m_low_word_layout # /home/dave/spring14/0x1-miniat/vm/src/miniat_priv_unions.h: 55

# No inserted files

